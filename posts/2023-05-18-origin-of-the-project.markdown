---
title: Origin of the project
---

## A long time ago in a galaxy far, far away

One day in 2017-2018 I was lurking a cryptocurrency forum.
Any specific information wasn't being searched.
Then the widely known in crypto circles Esperanto word appeared on my screen.
I saw it many times, but this time was different.
Suddenly, I got an insight into how to move forward the Black project that was started in 2014.
A lot of thought was given to it, resulting in names Blacknet, BlackCash and BlackGateway.

## An original idea

A financial privacy is desirable, but it's not necessarily tied to a core of a blockchain.
Blacknet provides a transparent blockchain, and BlackCash is implemented as an application that operates on its own fungible token.
An efficiency of privacy-preseving technologies is a worry.
One approach is to deal with a chosen anonymity subset.
In this case transaction data can be hidden as follows: a sender by a ring signature, an amount by a range proof, a reciever by a stealth address.
While it's possible in our environment, in practice end users are likely to send transactions in a privacy-leaking way.
Second approach is to deal with an anonymity set of all users.
In this case transaction data can be hidden by commitments and zero-knowledge proofs.
ZK-STARK is seen as a most promising solution despite proof sizes because it doesn't require a trusted setup.

## The present state

Blacknet works well, albeit without blockchain applications.
So I've resumed the exploration of BlackCash.
The zero-knowledge technology progressed, and modern zk-SNARKs don't require the trusted setup.
It was discovered that the privacy may actually enhance the securiry of the asynchronous proof of stake.
A possibility and implications of moving BlackCash into the base layer of Blacknet are being investigated.
